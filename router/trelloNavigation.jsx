import { createStackNavigator } from '@react-navigation/stack';
import { AddColumnView } from '../views/addColumnView';
import { AddTaskView } from '../views/addTaskView';
import ColumnView from '../views/columnView';
import { EditBoardView } from '../views/editBoardView';
import { EditColumnView } from '../views/editColumnView';
import { TaskRouteur } from './taskRouteur';
import { TrelloRouteur } from './trelloRouteur';

const Stack = createStackNavigator();

export default function TrelloNavigation() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="BoardView" component={TrelloRouteur} />
            <Stack.Screen name="EditBoardView" component={EditBoardView} />
            <Stack.Screen name="ColumnView" component={ColumnView} />
            <Stack.Screen name="EditColumnView" component={EditColumnView} />
            <Stack.Screen name="TaskRouteur" component={TaskRouteur} />
            <Stack.Screen name="AddColumnView" component={AddColumnView} />
            <Stack.Screen name="AddTaskView" component={AddTaskView} />
        </Stack.Navigator>
    );
}