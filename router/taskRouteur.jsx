import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import Ionicons from '@expo/vector-icons/Ionicons';
import { EditTaskView } from '../views/editTaskView';
import { TaskView } from '../views/taskView';

const Tab = createMaterialTopTabNavigator();

export function TaskRouteur({route}) {
    return (
        <Tab.Navigator
            tabBarPosition="top"
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color }) => {
                    let iconName;
                    let iconSize;

                    if (route.name === 'View') {
                        if (focused) {
                            iconName = 'apps';
                            iconSize = 24;
                        } else {
                            iconName = 'apps-outline';
                            iconSize = 18;

                        }
                    } else if (route.name === 'Edit') {
                        if (focused) {
                            iconName = 'pencil';
                            iconSize = 24;
                        } else {
                            iconName = 'pencil';
                            iconSize = 18;

                        }
                    }

                    // You can return any component that you like here!
                    return <Ionicons name={iconName} size={iconSize} color={color} />;
                },
                tabBarActiveTintColor: '#007bff',
                tabBarInactiveTintColor: 'gray',
            })}>
            <Tab.Screen name="View" component={TaskView} initialParams={{id: route.params.id, boardId: route.params.boardId, columnId: route.params.columnId, nom : route.params.nom, description: route.params.description, image: route.params.image}} />
            <Tab.Screen name="Edit" component={EditTaskView} initialParams={{id: route.params.id, boardId: route.params.boardId, columnId: route.params.columnId, nom : route.params.nom, description: route.params.description, image: route.params.image}} />
        </Tab.Navigator>

    )
}
