import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import { LoginView } from '../views/loginView';
import { RegisterView } from '../views/registerView';
const Tab = createMaterialTopTabNavigator()
export function AuthRouter() {
    return (
        <Tab.Navigator tabBarPosition="bottom">
            <Tab.Screen name="Login" component={LoginView} />
            <Tab.Screen name="Register" component={RegisterView} />
        </Tab.Navigator>
    )
}
