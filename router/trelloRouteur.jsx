import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import { BoardView } from '../views/boardView';
import { AddBoardView } from '../views/addBoardView';
import Ionicons from '@expo/vector-icons/Ionicons';

const Tab = createMaterialTopTabNavigator();

export function TrelloRouteur() {
    return (
        <Tab.Navigator
            tabBarPosition="top"
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color }) => {
                    let iconName;
                    let iconSize;

                    if (route.name === 'Board') {
                        if (focused) {
                            iconName = 'apps';
                            iconSize = 24;
                        } else {
                            iconName = 'apps-outline';
                            iconSize = 18;

                        }
                    } else if (route.name === 'AddBoard') {
                        if (focused) {
                            iconName = 'add-circle';
                            iconSize = 24;
                        } else {
                            iconName = 'add-circle-outline';
                            iconSize = 18;

                        }
                    }

                    // You can return any component that you like here!
                    return <Ionicons name={iconName} size={iconSize} color={color} />;
                },
                tabBarActiveTintColor: '#007bff',
                tabBarInactiveTintColor: 'gray',
            })}>
            <Tab.Screen name="Board" component={BoardView} />
            <Tab.Screen name="AddBoard" component={AddBoardView} />
        </Tab.Navigator>

    )
}
