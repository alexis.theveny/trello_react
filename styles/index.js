import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    contener: {
        flex: 1,
        alignItems: "flex-start"
    },
    safe: {
        marginTop: 40,
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    input: {
        height: 50,
        borderColor: "#000",
        borderWidth: 1,
        borderRadius: 8,
        paddingHorizontal: 8,
        width: 300
    },
    labelInput: {
        fontSize: 20,
        fontWeight: "bold"
    },
    contConnect: {
        // height: 425
        marginBottom: 40
    },
    btn: {
        height: 20,
        width: 300,
        backgroundColor: "#007bff",
        flex: 0.5,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 8,
        shadowColor: "#000000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.17,
        shadowRadius: 3.05,
        elevation: 4
    },
    btnDanger: {
        height: 20,
        width: 300,
        backgroundColor: "#DC3545",
        flex: 0.5,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 8,
        shadowColor: "#000000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.17,
        shadowRadius: 3.05,
        elevation: 4
    },
    btnPrimary: {
        height: 10,
        width: 300,
        backgroundColor: "#0D6EFD",
        flex: 0.5,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 8,
        shadowColor: "#000000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.17,
        shadowRadius: 3.05,
        elevation: 4
    },
    passmeter: {
        flex: 1,
        width: 300,
        height: 20,
        overflow: "hidden",
    },
    boards: {
        paddingHorizontal: 40,
        flex: 3, 
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
    },
    board: {
        backgroundColor: "#ffffff",
        borderWidth: 0.5,
        borderBottomColor : '#000',
        borderRightColor: '#000',
        borderTopColor: '#000',
        borderLeftWidth: 5,
        borderLeftColor: '#007bff',
        borderRadius: 10,
        overflow: "hidden",

        marginBottom: 20,
        width: '100%',
        height: 125,
        
        justifyContent: 'space-around',
        alignItems: 'center',
        shadowColor: "#000000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.17,
        shadowRadius: 3.05,
        elevation: 4
    },
    column: {
        backgroundColor: "#ffffff",
        borderWidth: 0.5,
        borderBottomColor : '#000',
        borderRightColor: '#000',
        borderTopColor: '#000',
        borderLeftWidth: 5,
        borderLeftColor: '#007bff',
        borderRadius: 10,
        overflow: "hidden",

        marginBottom: 20,
        width: '100%',
        height: 'auto',
        
        justifyContent: 'space-around',
        alignItems: 'center',
        shadowColor: "#000000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.17,
        shadowRadius: 3.05,
        elevation: 4
    },
    tasks: {
        padding:10,
        marginBottom:10,
        backgroundColor: "#ffffff",
        marginHorizontal: 10,
        borderRadius: 10,
    },
    task: {
        padding:10,
        marginBottom:10,
        backgroundColor: "#F8F8FF",
        borderRadius: 10,
    },
    addButton: {
        marginBottom: 20,
        padding: 20,
        marginHorizontal: 40,

        flex: 1,
        flexDirection:"row",
        justifyContent:"center",
        alignItems:"center",

        backgroundColor: "#ffffff",
        borderWidth: 1,
        borderColor: "#007bff",
        borderRadius:10,
        borderStyle:"dashed"
        
        
    }
})