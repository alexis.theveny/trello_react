import { StatusBar } from 'expo-status-bar';
import { useContext, useState } from 'react';
import { View, Alert, Keyboard, SafeAreaView, Text } from 'react-native'
import { editBoard, deleteBoard } from '../api/database';
import { MyBout } from '../components/bout';
import { MyInput } from '../components/input'
import { TrelloContext } from '../context';
import { styles } from '../styles';

export function EditBoardView({ route }) {
    const [board, setboard] = useState(route.params.name);
    const { user, setboards, boards } = useContext(TrelloContext);
    const modifyBoard = () => {
        Keyboard.dismiss()
        editBoard(user.uid, board, route.params.id).then(() => {
            Alert.alert('Tableau modifié')
            let newboards = [...boards];
            newboards[route.params.id] = {boardName:board};
            setboards(newboards)
        }).catch(err => Alert.alert(err))
    }
    const supprBoard = () => {
        Keyboard.dismiss()
        console.log(board);
        deleteBoard(user.uid, route.params.id).then(() => {
            Alert.alert('Tableau modifié')
            let newboards = []
            for (let i = 0; i < boards.length; i++) {
                if (i != route.params.id) {
                    if (boards[i]!= undefined) newboards.push(boards[i]);
                }
            }
            setboards(newboards)
        }).catch(err => Alert.alert(err))
    }
    return (
        <SafeAreaView style={styles.safe}>

            <View style={styles.contConnect}>
                <Text>Tableau : {route.params.id}</Text>
                <MyInput label={"board"} valeur={board} etat={setboard} />
                <MyBout label="Editer" click={modifyBoard} />
                <MyBout label="Supprimer" click={supprBoard} />

            </View>
            <StatusBar ></StatusBar>
        </SafeAreaView>

    )
}
