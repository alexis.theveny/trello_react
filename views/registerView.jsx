import { StatusBar } from 'expo-status-bar';
import { useContext, useState } from 'react';
import { View, Alert, SafeAreaView, Keyboard } from 'react-native'
import { register } from '../api/auth';
import { MyBout } from '../components/bout';
import { MyInput } from '../components/input'
import { TrelloContext } from '../context';
import { styles } from '../styles';

export function RegisterView() {
    const [login, setlogin] = useState("");
    const [mdp, setmdp] = useState("");
    const [mdp2, setmdp2] = useState("");
    const { setuser } = useContext(TrelloContext);
    const handleClick = () => {
        Keyboard.dismiss()
        if (mdp === mdp2) {
            register(login, mdp).then(data => {
                setuser(data);
            }).catch(err => Alert.alert(err))
        } else {
            Alert.alert("Les mots de passe sont differents")
        }
    }
    return (
        <SafeAreaView style={styles.safe}>


            <View style={[styles.contConnect, {justifyContent:"center"}]}>
                <View style={{ alignItems:"center", height: 400}}>
                <MyInput label={"Email"} valeur={login} etat={setlogin} />
                <MyInput label={"Password"} valeur={mdp} etat={setmdp} type="password-register"/>
                <MyInput label={"Confirm password"} valeur={mdp2} etat={setmdp2} type="password-register"/>
                <MyBout label="Register" click={handleClick} />
                </View>

            </View>
            <StatusBar ></StatusBar>
        </SafeAreaView>

    )
}
