import { useContext, useEffect } from "react";
import { SafeAreaView, View, ScrollView } from "react-native";
import { getBoards } from "../api/database";
import { Board } from "../components/board";
import { TrelloContext } from "../context";
import { styles } from "../styles";

export function BoardView({navigation}) {

    const { user, boards, setboards } = useContext(TrelloContext);
    useEffect(() => {
        getBoards(user.uid).then(data => {
            setboards(data);
        })

    }, []);
    return (
        <SafeAreaView style={styles.safe}>
            <ScrollView>
                <View style={styles.boards}>
                    {boards.map((elem, key) => <Board key={key} nom={elem.boardName} navigation={navigation} id={key} />)}
                </View>

            </ScrollView>
        </SafeAreaView>
    )
}
