import { useContext, useEffect } from "react";
import { Image } from "react-native";
import { SafeAreaView, View, ScrollView, Text } from "react-native";
import { getBoards } from "../api/database";
import { Board } from "../components/board";
import { TrelloContext } from "../context";
import { styles } from "../styles";

export function TaskView({ route }) {

    const { tasks, columns } = useContext(TrelloContext);
    return (
        <SafeAreaView style={styles.safe}>
            <ScrollView>
                <View style={{paddingHorizontal: 40, flexDirection:"column"}}>
                        <Text style={{ fontSize: 24, fontWeight: "bold" }}>Titre : {route.params.nom}</Text>
                        <Text style={{ fontSize: 14, fontWeight: "normal" }}>Description : {route.params.description}</Text>
                        {route.params.image && <Image source={{ uri: route.params.image }} style={{ width: 200, height: 200, justifyContent: "center" }} />}

                        <Text style={{ fontSize: 14, fontWeight: "normal" }}>Colonne : {columns[route.params.columnId].columnName}</Text>
                </View>

            </ScrollView>
        </SafeAreaView>
    )
}
