import { useContext, useEffect, React } from "react";
import { SafeAreaView, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { getColumns } from "../api/database";
import { Column } from "../components/column";
import { TrelloContext } from "../context";
import { styles } from "../styles";
import Ionicons from '@expo/vector-icons/Ionicons';
import { Alert } from "react-native";

export default function ColumnView({ route, navigation }) {
    const { user, columns, setcolumns, tasks, edit, setedit } = useContext(TrelloContext);

    useEffect(() => {
        getColumns(user.uid, route.params.id).then(data => {
            setcolumns(data);
            setedit(!edit)
        })
    }, [tasks]);

    return (
        <SafeAreaView style={styles.safe}>
            <ScrollView>
                    <TouchableOpacity
                        style={[styles.addButton]}
                        onPress={() => navigation.push("AddColumnView", { boardId: route.params.id })}
                    >
                        <Ionicons name="add-circle" size={24} color="#007bff" />
                        <Text style={{ color: "#007bff", paddingLeft: 20 }}>Add column</Text>
                    </TouchableOpacity>
                <View style={styles.boards}>
                    {columns.map((elem, key) => <Column key={key} nom={elem.columnName} navigation={navigation} id={key} boardId={route.params.id} />)}
                </View>
            </ScrollView>
        </SafeAreaView >
    );
}