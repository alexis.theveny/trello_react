import { StatusBar } from 'expo-status-bar';
import { useContext, useState } from 'react';
import { View, Alert, Keyboard, SafeAreaView, Button, Image } from 'react-native'
import { addTask } from '../api/database';
import { MyBout } from '../components/bout';
import { MyInput } from '../components/input'
import { TrelloContext } from '../context';
import { styles } from '../styles';
import * as ImagePicker from 'expo-image-picker';
import { getDownloadURL, getStorage, ref as refB, uploadBytes } from "firebase/storage";
import { app } from "../api/app";
const storage = getStorage(app);

export function AddTaskView({ route }) {
    const [task, settask] = useState("");
    const [description, setdescription] = useState("");
    const [image, setImage] = useState(null);

    const { user, settasks, tasks, columns } = useContext(TrelloContext);


    let columnsList = []
    for (let i = 0; i < columns.length; i++) {
        columnsList.push(columns[i].columnName);
    }

    const handleClick = () => {
        Keyboard.dismiss()
        console.log(route.params.boardId);
        addTask(user.uid, route.params.boardId, route.params.columnId, task, description, image).then(() => {
            Alert.alert('Tache ajoutée')
            settasks([...tasks, { taskName: task, taskDescription: description, image: image }])
        }).catch(err => Alert.alert(err))
    }


    const pickImage = async () => {
        // No permissions request is necessary for launching the image library
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            quality: 1,
        });
        if (!result.cancelled) {
            uploadFile(result.uri).then((uri) => {
                console.log(uri);
                setImage(uri);
                Alert.alert("Import success")
            }).catch(err => {
                Alert.alert("Import error")
                console.log(err)
            })
        }

        console.log(result);

    };

    function uploadFile(uri) {
        return new Promise((res, rej) => {
            // Why are we using XMLHttpRequest? See:
            // https://github.com/expo/expo/issues/2402#issuecomment-443726662
            new Promise((resolve, reject) => {
                const xhr = new XMLHttpRequest();
                xhr.onload = function () {
                    resolve(xhr.response);
                };
                xhr.onerror = function (e) {
                    console.log(e);
                    reject(new TypeError("Network request failed"));
                };
                xhr.responseType = "blob";
                xhr.open("GET", uri, true);
                xhr.send(null);
            }).then(blob => {
                console.log(blob);
                console.log(storage);
                const fileRef = refB(storage, `${user.uid}_${route.params.boardId}_${route.params.columnId}_${route.params.id}`);
                uploadBytes(fileRef, blob).then(snapshot => {
                    // We're done with the blob, close and release it
                    blob.close();
                    res(getDownloadURL(fileRef))
                })
            }).catch(err => rej(err.message))

        })
    }

    return (
        <SafeAreaView style={styles.safe}>

            <View style={[styles.contConnect, { alignItems: "center", justifyContent:"space-between" }]}>
                <View style={{flex:2, marginBottom: 40, alignItems: "center",}}>
                    <MyInput label={"task"} valeur={task} etat={settask} />
                    <MyInput label={"description"} valeur={description} etat={setdescription} />
                    <Button title={image ? "Change image" : "Add image to task"} onPress={pickImage} style={{ marginBottom: 20 }} />
                    {image && <Image source={{ uri: image }} style={{ width: 200, height: 200, justifyContent: "center" }} />}
                </View>
                <MyBout label="Valider" click={handleClick} />

            </View>
            <StatusBar ></StatusBar>
        </SafeAreaView>

    )
}
