import { StatusBar } from 'expo-status-bar';
import { useContext, useState } from 'react';
import { View, Alert, Keyboard, SafeAreaView } from 'react-native'
import { addColumn } from '../api/database';
import { MyBout } from '../components/bout';
import { MyInput } from '../components/input'
import { TrelloContext } from '../context';
import { styles } from '../styles';
import SelectDropdown from 'react-native-select-dropdown'

export function AddColumnView({route}) {
    const [column, setcolumn] = useState("");
    const { user, setcolumns, columns } = useContext(TrelloContext);
    const handleClick = () => {
        Keyboard.dismiss()
        // console.log("column", column);
        addColumn(user.uid, route.params.boardId, column).then(() => {
            Alert.alert('Colonne ajoutée')
            setcolumns([...columns, {columnName: column}])
        }).catch(err => Alert.alert(err))
        // connectUser(login, mdp).then(data => {
        //     setuser(data);
        // }).catch(err => Alert.alert(err))
    }
    return (
        <SafeAreaView style={styles.safe}>

            <View style={styles.contConnect}>
                <MyInput label={"column"} valeur={column} etat={setcolumn} />
                <MyBout label="Valider" click={handleClick} />

            </View>
            <StatusBar ></StatusBar>
        </SafeAreaView>

    )
}
