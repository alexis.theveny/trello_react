import { StatusBar } from 'expo-status-bar';
import { useContext, useEffect, useState } from 'react';
import { Animated } from 'react-native';
import { View, Alert, Keyboard, SafeAreaView, Image } from 'react-native'
import { login } from '../api/auth';
import { MyBout } from '../components/bout';
import { MyInput } from '../components/input'
import { TrelloContext } from '../context';
import { styles } from '../styles';
import React, { useRef } from 'react';

export function LoginView() {
    const [email, setemail] = useState("");
    const [mdp, setmdp] = useState("");
    const { setuser } = useContext(TrelloContext);

    useEffect(() => {
        deplace()
      }, []);

    const handleClick = () => {
        Keyboard.dismiss()
        login(email, mdp).then(data => {
            setuser(data);
        }).catch(err => Alert.alert(err))
    }
    const valeur = useRef(new Animated.Value(0)).current
    const deplace = () => {
        Animated.timing(valeur, {
            toValue: 500,
            duration: 2000,
            useNativeDriver: false
        }).start()
    }
    return (
        <SafeAreaView style={styles.safe}>

            <Animated.View style={[styles.contConnect, { paddingVertical: 200, alignItems: "center" }, valeur.getLayout()]}>
                <View style={{ alignItems: "center", height: 400 }}>
                    <MyInput label={"Email"} valeur={email} etat={setemail} />
                    <MyInput label={"Password"} valeur={mdp} etat={setmdp} type="password-login" />
                    <MyBout label="Connect" click={handleClick} />
                    <Image source={{ uri: 'https://cdn-icons-png.flaticon.com/512/2702/2702602.png' }} style={{ width: 50, height: 50, marginTop: 20 }} />
                </View>
            </Animated.View>
            <StatusBar ></StatusBar>
        </SafeAreaView>

    )
}
