import { StatusBar } from 'expo-status-bar';
import { View, Alert, Keyboard, SafeAreaView } from 'react-native'
import SelectDropdown from 'react-native-select-dropdown'
import { useContext, useState } from 'react';
import { deleteTask, addTask, editTask } from '../api/database';
import { MyBout } from '../components/bout';
import { MyInput } from '../components/input'
import { TrelloContext } from '../context';
import { styles } from '../styles';
import * as ImagePicker from 'expo-image-picker';
import { getDownloadURL, getStorage, ref as refB, uploadBytes } from "firebase/storage";
import { app } from "../api/app";
import { Image } from 'react-native';
import { Button } from 'react-native';
const storage = getStorage(app);


export function EditTaskView({ route }) {
    const { user, settasks, tasks, columns } = useContext(TrelloContext);

    const [task, settask] = useState(route.params.nom);
    const [description, setdescription] = useState(route.params.description);
    const [image, setImage] = useState(route.params.image);
    const [column, setcolumn] = useState(route.params.columnId);

    const modifyTask = () => {
        Keyboard.dismiss()
        let newtasks = []
        if (column != route.params.columnId) {
            deleteTask(user.uid, route.params.boardId, route.params.columnId, route.params.id).then(() => {
                for (let i = 0; i < tasks.length; i++) {
                    if (i != route.params.id) {
                        if (tasks[i] != undefined) newtasks.push(tasks[i]);
                    }
                }
                settasks(newtasks)
            }).catch(err => Alert.alert(err))

            addTask(user.uid, route.params.boardId, column, task, description, image).then(() => {
                settasks([...newtasks, { taskName: task, taskDescription: description, image: image}])
            }).catch(err => Alert.alert(err))
        } else {
            editTask(user.uid, route.params.boardId, route.params.columnId, route.params.id, task, description, image).then(() => {
                newtasks = [...tasks]
                newtasks[route.params.id] = { taskName: task, taskDescription: description, image: image };
                settasks(newtasks)
            })
        }

        Alert.alert('Tache modifiée')
    }

    const supprTask = () => {
        Keyboard.dismiss()
        deleteTask(user.uid, route.params.boardId, route.params.columnId, route.params.id).then(() => {
            Alert.alert('Tache supprimée')
            let newtasks = []
            for (let i = 0; i < tasks.length; i++) {
                if (i != route.params.id) {
                    if (tasks[i] != undefined) newtasks.push(tasks[i]);
                }
            }
            settasks(newtasks)
        }).catch(err => Alert.alert(err))
    }
    const pickImage = async () => {
        // No permissions request is necessary for launching the image library
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            quality: 1,
        });
        if (!result.cancelled) {
            uploadFile(result.uri).then((uri) => {
                console.log(uri);
                setImage(uri);
                Alert.alert("Import success")
            }).catch(err => {
                Alert.alert("Import error")
                console.log(err)
            })
        }

        console.log(result);

    };

    function uploadFile(uri) {
        return new Promise((res, rej) => {
            // Why are we using XMLHttpRequest? See:
            // https://github.com/expo/expo/issues/2402#issuecomment-443726662
            new Promise((resolve, reject) => {
                const xhr = new XMLHttpRequest();
                xhr.onload = function () {
                    resolve(xhr.response);
                };
                xhr.onerror = function (e) {
                    console.log(e);
                    reject(new TypeError("Network request failed"));
                };
                xhr.responseType = "blob";
                xhr.open("GET", uri, true);
                xhr.send(null);
            }).then(blob => {
                console.log(blob);
                console.log(storage);
                const fileRef = refB(storage, `${user.uid}_${route.params.boardId}_${route.params.columnId}_${route.params.id}`);
                uploadBytes(fileRef, blob).then(snapshot => {
                    // We're done with the blob, close and release it
                    blob.close();
                    res(getDownloadURL(fileRef))
                })
            }).catch(err => rej(err.message))

        })
    }



    let columnsList = []
    for (let i = 0; i < columns.length; i++) {
        columnsList.push(columns[i].columnName);
    }
    return (
        <SafeAreaView style={styles.safe}>

            <View style={styles.contConnect}>
                <MyInput label={"task"} valeur={task} etat={settask} />
                <MyInput label={"description"} valeur={description} etat={setdescription} />
                <Button title={image ? "Change image" : "Add image to task"} onPress={pickImage} style={{ marginBottom: 20 }} />
                {image && <Image source={{ uri: image }} style={{ width: 200, height: 200, justifyContent: "center" }} />}


                <SelectDropdown
                    data={columnsList}
                    onSelect={(selectedItem, index) => {
                        setcolumn(index)
                    }}
                    buttonTextAfterSelection={(selectedItem, index) => {
                        return selectedItem
                    }}
                    rowTextForSelection={(item, index) => {
                        return item
                    }}
                />
                <MyBout label="Editer" click={modifyTask} />
                <MyBout label="Supprimer" click={supprTask} />

            </View>
            <StatusBar ></StatusBar>
        </SafeAreaView>

    )
}
