import { StatusBar } from 'expo-status-bar';
import { useContext, useState } from 'react';
import { View, Alert, Keyboard, SafeAreaView } from 'react-native'
import { addBoard } from '../api/database';
import { MyBout } from '../components/bout';
import { MyInput } from '../components/input'
import { TrelloContext } from '../context';
import { styles } from '../styles';

export function AddBoardView() {
    const [board, setboard] = useState("");
    const { user, setboards, boards } = useContext(TrelloContext);
    const handleClick = () => {
        Keyboard.dismiss()
        //    console.log(board);
        addBoard(user.uid, board).then(() => {
            Alert.alert('Tableau ajouté')
            setboards([...boards, {boardName:board}])
        }).catch(err => Alert.alert(err))
        // connectUser(login, mdp).then(data => {
        //     setuser(data);
        // }).catch(err => Alert.alert(err))
    }
    return (
        <SafeAreaView style={styles.safe}>

            <View style={styles.contConnect}>
                <MyInput label={"board"} valeur={board} etat={setboard} />
                <MyBout label="Valider" click={handleClick} />

            </View>
            <StatusBar ></StatusBar>
        </SafeAreaView>

    )
}
