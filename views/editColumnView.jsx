import { StatusBar } from 'expo-status-bar';
import { useContext, useState } from 'react';
import { View, Alert, Keyboard, SafeAreaView, Text } from 'react-native'
import { editColumn, deleteColumn } from '../api/database';
import { MyBout } from '../components/bout';
import { MyInput } from '../components/input'
import { TrelloContext } from '../context';
import { styles } from '../styles';

export function EditColumnView({ route }) {
    const [column, setcolumn] = useState("");
    const { user, setcolumns, columns } = useContext(TrelloContext);

    const modifyColumn = () => {
        Keyboard.dismiss()
        editColumn(user.uid, route.params.boardId, column, route.params.columnId, ).then(() => {
            Alert.alert('Colonne modifiée')
            let newcolumns = [...columns];
            newcolumns[route.params.columnId] = {columnName:column};
            setcolumns(newcolumns)
        }).catch(err => Alert.alert(err))
        
    }
    const supprColumn = () => {
        Keyboard.dismiss()
        deleteColumn(user.uid, route.params.boardId, route.params.columnId).then(() => {
            Alert.alert('Colonne supprimée')
            let newcolumns = []
            for (let i = 0; i < columns.length; i++) {
                if (i != route.params.columnId) {
                    if (columns[i]!= undefined) newcolumns.push(columns[i]);
                }
            }
            setcolumns(newcolumns)
        }).catch(err => Alert.alert(err))
    }
    return (
        <SafeAreaView style={styles.safe}>

            <View style={styles.contConnect}>
                <MyInput label={"column"} valeur={column} etat={setcolumn} />
                <MyBout label="Editer" click={modifyColumn} />
                <MyBout label="Supprimer" click={supprColumn} />
            </View>
            <StatusBar ></StatusBar>
        </SafeAreaView>

    )
}
