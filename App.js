import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native'
import { AuthRouter } from './router/authRouter';
import { TrelloContext } from './context';
import TrelloNavigation from './router/trelloNavigation';

export default function App() {
  const [user, setuser] = useState(null);
  const [boards, setboards] = useState([]);
  const [columns, setcolumns] = useState([]);
  const [tasks, settasks] = useState([]);
  const [edit, setedit] = useState(false);
  return (
    <TrelloContext.Provider value={{ user, setuser, boards, setboards, columns, setcolumns, tasks, settasks, edit, setedit }} >
      <NavigationContainer>
        {(user) ? <TrelloNavigation></TrelloNavigation> : <AuthRouter></AuthRouter>}
      </NavigationContainer>
    </TrelloContext.Provider>
  );
}

