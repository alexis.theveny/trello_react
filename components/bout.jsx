import { TouchableOpacity, Text } from 'react-native'
import { styles } from '../styles'
export function MyBout({ label, click }) {
    return (
        <TouchableOpacity style={[label == "Supprimer" ? styles.btnDanger : styles.btnPrimary]} onPress={click}>
            <Text style={[styles.labelInput, {color:"#ffffff"}]}>{label}</Text>
        </TouchableOpacity>
    )
}