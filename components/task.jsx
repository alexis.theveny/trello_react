import { TouchableOpacity, Text, View } from "react-native";
import { styles } from "../styles";


export function Task({ nom, description,image, id, boardId, columnId, navigation }) {
    return (
        <TouchableOpacity 
            onPress={()=> navigation.push("TaskRouteur", {id: id, boardId: boardId, columnId: columnId, nom: nom, description: description, image: image})}
            style={styles.task}
        >
            <Text style={{fontWeight:"500", fontSize:14}}>{nom}</Text>
            <Text style={{fontWeight:"200", fontSize:12}}>{description}</Text>
        </TouchableOpacity>
    )
}
