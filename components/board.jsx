import { useContext, useState } from "react";
import { TouchableOpacity, Text, Alert, View } from "react-native";
import { TrelloContext } from '../context';
import { deleteBoard } from '../api/database';
import { styles } from "../styles";

export function Board({ nom, id, navigation }) {
    const [board, setboard] = useState(nom);
    const { user, setboards, boards } = useContext(TrelloContext);

    const supprBoard = () => {
        console.log(board);
        deleteBoard(user.uid, id).then(() => {
            Alert.alert('Tableau modifié')
            let newboards = []
            for (let i = 0; i < boards.length; i++) {
                if (i != id) {
                    if (boards[i]!= undefined) newboards.push(boards[i]);
                }
            }
            setboards(newboards)
        }).catch(err => Alert.alert(err))
    }

    return (
        <TouchableOpacity
            style={styles.board}
            onPress={() => navigation.push("ColumnView", { id: id })}
        >
            <Text style={{flex:3, width: '100%', textAlign:"center", textAlignVertical:"center"}}>{nom}</Text>
            <View style={{flex:1, flexDirection:'row', justifyContent:'flex-start', width: '100%'}}>
                <TouchableOpacity
                style={{width:'50%', flex: 1, justifyContent:"center", alignItems:"center", backgroundColor:"#0D6EFD"}}
                    onPress={() => navigation.push("EditBoardView", { id: id, name: nom })}
                >
                    <Text style={{color: "#ffffff", fontWeight:"bold"}}>Editer</Text>
                </TouchableOpacity>
                <TouchableOpacity
                style={{width:'50%', flex: 1, justifyContent:"center", alignItems:"center", backgroundColor:"#DC3545"}}
                    onPress={() => supprBoard()}
                >
                    <Text style={{color: "#ffffff", fontWeight:"bold"}}>Supprimer</Text>
                </TouchableOpacity>
            </View>
        </TouchableOpacity>
    )
}
