import * as React from 'react';
import { View, Text, TextInput, ViewBase } from 'react-native'
import { styles } from '../styles'
import { PasswordMeter } from 'react-native-password-meter';

export function MyInput({ type = "", label, valeur, etat }) {
    const [passwordScore, setPasswordScore] = React.useState(0);
    const _updateScore = (val) => {
        setPasswordScore(val);
    };

    if (type == "password-register") {
        return (
            <View style={styles.contener}>
                <Text style={styles.labelInput}>{label}</Text>
                <TextInput value={valeur} onChangeText={etat}
                    style={styles.input}
                    secureTextEntry
                />
                <View style={styles.passmeter}>
                    <PasswordMeter
                        style={styles.passmeterBar}
                        password={valeur}
                        onResult={(val) => {
                            _updateScore(val);
                        }}
                    />
                </View>
            </View>
        )
    } else if (type == "password-login") {
        return (
            <View style={styles.contener}>
                <Text style={styles.labelInput}>{label}</Text>
                <TextInput value={valeur} onChangeText={etat}
                    style={styles.input}
                    secureTextEntry
                />
            </View>
        )
    } else {
        return (
            <View style={styles.contener}>
                <Text style={styles.labelInput}>{label}</Text>
                <TextInput value={valeur} onChangeText={etat} style={styles.input} />
            </View>
        )
    }

}
