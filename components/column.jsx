import { TouchableOpacity, Text, View } from "react-native";
import { styles } from "../styles";
import { Collapse, CollapseHeader, CollapseBody } from 'accordion-collapse-react-native';
import { Task } from "./task";
import Ionicons from '@expo/vector-icons/Ionicons';
import { useContext, useEffect, useState } from "react";
import { getTasks } from "../api/database";
import { TrelloContext } from "../context";



export function Column({ nom, id, boardId, navigation }) {
    const { user, edit } = useContext(TrelloContext);
    const [tasklist, settasklist] = useState("")

    useEffect(() => {
        getTasks(user.uid, boardId, id).then(data => {
            settasklist(data);
        })
    }, [edit]);
    return (
        <View style={styles.column}>
            <Collapse isExpanded="true" style={{ width: "100%" }}>
                <CollapseHeader style={{ paddingHorizontal: 20, width: "100%", paddingVertical: 20 }}>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                            <Text style={{ fontWeight: "bold", fontSize: 16, color: "#007bff" }}>{nom}</Text>
                            <TouchableOpacity
                                style={{ marginLeft: 10 }}
                                onPress={() => navigation.push("EditColumnView", { boardId: boardId, columnName: nom, columnId: id })}
                            >
                                <Ionicons name="pencil" size={20} color="#00000075" />
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity
                            onPress={() => navigation.push("AddTaskView", { boardId: boardId, columnId: id })}
                            style={{ flexDirection: "row", alignItems: "center", borderWidth: 1, borderRadius: 15, borderColor: "#007bff", padding: 5 }}
                        >
                            <Ionicons name="add-circle" size={24} color="#007bff" />
                            <Text style={{ color: "#007bff" }}>Add Tasks</Text>
                        </TouchableOpacity>
                    </View>
                </CollapseHeader>
                <CollapseBody>

                    <View style={styles.tasks}>
                        {tasklist.length > 0
                            ? tasklist.map((elem,key) => <Task key={key} boardId={boardId} columnId={id} nom={elem.taskName} description={elem.taskDescription} image={elem.image} id={key} navigation={navigation} />)
                            : <Text style={{ fontWeight: "200" }}>No tasks found</Text>
                        }
                    </View>
                </CollapseBody>
            </Collapse>
        </View >
        // <TouchableOpacity 
        //     style={styles.column}
        //     // onPress={() => navigation.push("ColumnView", {id : id})}
        //     // onLongPress={() => navigation.push("EditBoardView", {id : id, name: nom})}
        // >
        //     <Text>{nom}</Text>
        // </TouchableOpacity>
    )
}
