// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCpZ1A5ES1urDd7f6avEWIN4K7nRPmvQKI",
  authDomain: "trello-cf713.firebaseapp.com",
  projectId: "trello-cf713",
  storageBucket: "trello-cf713.appspot.com",
  messagingSenderId: "1063721308450",
  appId: "1:1063721308450:web:e274291c9353cc3843557b",
  databaseURL: "https://trello-cf713-default-rtdb.europe-west1.firebasedatabase.app"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);