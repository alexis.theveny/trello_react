import { child, get, getDatabase, ref, set } from "firebase/database";
import { app } from "./app";

const database = getDatabase(app);

///////////
// BOARDS
///////////
export function getBoards(userId) {
    return new Promise((res, rej) => {
        const refDb = ref(database)
        const refUser = child(refDb, `users/${userId}`)
        get(refUser).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            res(data)
        }).catch(err => rej(err.message))
    })
}
export function addBoard(userId, name) {
    return new Promise((res, rej) => {

        const refDb = ref(database)
        const refUser = child(refDb, `users/${userId}`)
        get(refUser).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            data.push({boardName : name})
            set(refUser, data);
            res(true)
        }).catch(err => rej(err.message))
    })
}
export function editBoard(userId, name, id) {
    return new Promise((res, rej) => {

        const refDb = ref(database)
        const refUser = child(refDb, `users/${userId}`)
        get(refUser).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            data[id].boardName = name;
            set(refUser, data);
            res(true)
        }).catch(err => rej(err.message))
    })
}
export function deleteBoard(userId, id) {
    return new Promise((res, rej) => {

        const refDb = ref(database)
        const refUser = child(refDb, `users/${userId}`)
        get(refUser).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            let newdata = []
            for (let i = 0; i <= data.length; i++) {
                if (i != id) {
                    if (data[i]!= undefined) newdata.push(data[i]);
                }
            }
            set(refUser, newdata);
            res(true)
        }).catch(err => rej(err.message))
    })
}

///////////
// COLUMNS
///////////
export function getColumns(userId, boardId) {
    return new Promise((res, rej) => {
        const refDb = ref(database)
        const refBoard = child(refDb, `users/${userId}/${boardId}/columns`)
        get(refBoard).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            res(data)
        }).catch(err => rej(err.message))
    })
}
export function addColumn(userId, boardId, name) {
    return new Promise((res, rej) => {
        
        const refDb = ref(database)
        const refBoard = child(refDb, `users/${userId}/${boardId}/columns`)
        get(refBoard).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            data.push({columnName:name})
            set(refBoard, data);
            res(true)
        }).catch(err => rej(err.message))
    })
    
}
export function editColumn(userId, boardId, name, id) {
    return new Promise((res, rej) => {
        
        const refDb = ref(database)
        const refBoard = child(refDb, `users/${userId}/${boardId}/columns`)
        get(refBoard).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            // console.log(data);
            data[id].columnName = name;
            // data[id] = {columnName:name};

            set(refBoard, data);
            res(true)
        }).catch(err => rej(err.message))
    })
}
export function deleteColumn(userId, boardId, id) {
    return new Promise((res, rej) => {
        
        const refDb = ref(database)
        const refBoard = child(refDb, `users/${userId}/${boardId}/columns`)
        get(refBoard).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            let newdata = []
            for (let i = 0; i <= data.length; i++) {
                if (i != id) {
                    if (data[i]!= undefined) newdata.push(data[i]);
                }
            }
            set(refBoard, newdata);
            res(true)
            deleteTasks(userId, boardId, id)
        }).catch(err => rej(err.message))
    })
}

///////////
// TASKS
///////////
export function getTasks(userId, boardId, columnId) {
    return new Promise((res, rej) => {
        const refDb = ref(database)
        const refBoard = child(refDb, `users/${userId}/${boardId}/columns/${columnId}/tasks`)
        get(refBoard).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            res(data)
        }).catch(err => rej(err.message))
    })
}
export function addTask(userId, boardId, columnId, name, description, image) {
    return new Promise((res, rej) => {

        const refDb = ref(database)
        const refBoard = child(refDb, `users/${userId}/${boardId}/columns/${columnId}/tasks`)
        console.log("Board URL :",refBoard);
        get(refBoard).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            console.log("Data columns :",data);
            data.push({taskName:name, taskDescription:description, image: image})
            console.log(data);
            set(refBoard, data);
            res(true)
        }).catch(err => rej(err.message))
    })

}

export function editTask(userId, boardId, columnId, taskId, name, description, image) {
    return new Promise((res, rej) => {
        
        const refDb = ref(database)
        const refBoard = child(refDb, `users/${userId}/${boardId}/columns/${columnId}/tasks`)
        get(refBoard).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            // console.log(data);
            data[taskId] = {taskName:name, taskDescription:description, image: image};
            set(refBoard, data);
            res(true)
        }).catch(err => rej(err.message))
    })
}
export function deleteTask(userId, boardId, columnId, id) {
    return new Promise((res, rej) => {
        
        const refDb = ref(database)
        const refBoard = child(refDb, `users/${userId}/${boardId}/columns/${columnId}/tasks`)
        get(refBoard).then(snap => {
            let data = snap.val()
            if (data == "" || data === null) data = []
            let newdata = []
            for (let i = 0; i <= data.length; i++) {
                if (i != id) {
                    if (data[i]!= undefined) newdata.push(data[i]);
                }
            }
            set(refBoard, newdata);
            res(true)
        }).catch(err => rej(err.message))
    })
}
